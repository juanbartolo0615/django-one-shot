from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todos_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todos": todos,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            post = form.save(False)
            form.save()
            return redirect("todo_list_detail", post.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            print(form)
            form.save()
            return redirect(
                "todo_list_detail",
            )
    else:
        form = TodoListForm(instance=post)
    context = {
        "post": post,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todos_list")
    context = {
        "todo": todo,
    }
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todos_list")
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/itemAdd.html", context)


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", post.list.id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "post": post,
        "form": form,
    }
    return render(request, "todos/itemUpdate.html", context)
